﻿using RemoteCourses.Models;
using System.Collections.Generic;

namespace RemoteCourses.Interfaces
{
    interface ICoursesRepository : IRepository<Course>
    {

    }
}
