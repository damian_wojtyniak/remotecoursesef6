﻿using RemoteCourses.Models;

namespace RemoteCourses.Interfaces
{
    interface IContentsRepository : IRepository<Content>
    {

    }
}
