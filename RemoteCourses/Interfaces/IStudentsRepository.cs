﻿using RemoteCourses.Models;

namespace RemoteCourses.Interfaces
{
    interface IStudentsRepository : IRepository<Student>
    {

    }
}
