﻿using RemoteCourses.Interfaces;
using RemoteCourses.Models;
using RemoteCourses.Repositories;
using RemoteCourses.Repositories.EntityFramework;
using System.Windows;
using System.Windows.Input;

namespace RemoteCourses.Views
{
    public partial class MainWindow : Window
    {
        private IUnitOfWork unitOfWork;

        public MainWindow()
        {
            InitializeComponent();

            unitOfWork = new UnitOfWork(new CourseContext());

            DataContext = unitOfWork;

            //using (unitOfWork)
            //{
            //    unitOfWork.Students.Add(new Student() { Name = "Test" });
            //    unitOfWork.Save();
            //}
        }

        private void RefreshCoursesButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshCourses();
        }

        private void RefreshStudentsButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshStudents();
        }

        private void RefreshStudents()
        {
            var students = unitOfWork.Students.GetAll();

            dataGridStudents.ItemsSource = null;
            dataGridStudents.ItemsSource = students;
        }

        private void RefreshCourses()
        {
            var courses = unitOfWork.Courses.GetAll();

            dataGridCourseList.ItemsSource = null;
            dataGridCourseList.ItemsSource = courses;
        }

        private void StudentsMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            unitOfWork.Results.GetAll();

            var student = dataGridStudents.SelectedItem as Student;
            var quizResults = student.Results;

            dataGridQuiz.ItemsSource = null;
            dataGridQuiz.ItemsSource = quizResults;
        }

        private void CoursesMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            unitOfWork.Sections.GetAll();

            var course = dataGridCourseList.SelectedItem as Course;
            var section = course.Section;

            dataGridSection.ItemsSource = null;
            dataGridSection.ItemsSource = section;
        }

        private void AddStudentButtonClick(object sender, RoutedEventArgs e)
        {
            unitOfWork.Students.GetAll();

            var providedText = addStudentTextBox.Text.Trim();
            
            var newStudent = new Student();
            newStudent.Name = providedText;

            unitOfWork.Students.Add(newStudent);
            unitOfWork.Save();

            addStudentTextBox.Text = "";
            RefreshStudents();
        }

        private void DeleteStudentButtonClick(object sender, RoutedEventArgs e)
        {
            unitOfWork.Students.GetAll();

            var selectedStudent = dataGridStudents.SelectedItem as Student;
            unitOfWork.Students.Remove(selectedStudent);
            unitOfWork.Save();
            
            RefreshStudents();
        }

        private void UpdateStudentButtonClick(object sender, RoutedEventArgs e)
        {
            unitOfWork.Students.GetAll();

            var selectedStudent = dataGridStudents.SelectedItem as Student;
            unitOfWork.Students.Update(selectedStudent);
            unitOfWork.Save();

            RefreshStudents();
        }
    }
}
