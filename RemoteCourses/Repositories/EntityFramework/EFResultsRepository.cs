﻿using RemoteCourses.Interfaces;
using RemoteCourses.Models;

namespace RemoteCourses.Repositories.EntityFramework
{
    class EFResultsRepository : Repository<Result>, IResultsRepository
    {
        public EFResultsRepository(CourseContext context) : base(context) { }

        public CourseContext CoursesContext
        {
            get { return Context as CourseContext; }
        }
    }
}
