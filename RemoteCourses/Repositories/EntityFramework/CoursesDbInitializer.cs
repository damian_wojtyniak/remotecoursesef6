﻿using RemoteCourses.Models;
using System.Collections.Generic;
using System.Data.Entity;

namespace RemoteCourses.Repositories.EntityFramework
{
    class CoursesDbInitializer : CreateDatabaseIfNotExists<CourseContext>
    {
        protected override void Seed(CourseContext context)
        {
            var coursesEntries = new List<Course>()
            {
                new Course
                {
                    Title = "Gry karciane",
                    Description = "Kurs poświęcony nauce gry karcianej \"Poker\""
                },
                new Course
                {
                    Title = "Nauka gry w bierki",
                    Description = "Kurs poświęcony nauce gry \"Bierki\""
                },
            };

            var studentEntries = new List<Student>()
            {
                new Student
                {
                    Id = 1,
                    Name = "Damian",
                    Password = "test",
                    Email = "damian@test.com",
                },
                new Student
                {
                    Id = 2,
                    Name = "Mariusz",
                    Password = "test",
                    Email = "mariusz@test.com"
                }
            };

            var sectionEntries = new List<Section>()
            {
                new Section
                {
                    SectionName = "Podstawy Pokera"
                },
                new Section
                {
                    SectionName = "Podstawy bierek"
                }
            };

            var contentsEntries = new List<Content>()
            {
                new Content
                {
                    Title = "Podstawy pokera",
                    ContentText = "To dobra gra i na pewno umiesz w nią grać :)"
                },
                new Content
                {
                    Title = "Podstawy bierek",
                    ContentText = "To dobra gra i na pewno umiesz w nią grać :)"
                },
            };

            var questionsEntries = new List<Question>()
            {
                new Question
                {
                    Text = "Jaki układ kart jest najsilniejszy w grze w pokera?",
                    CorrectAnswer = "Poker"
                },
                new Question
                {
                    Text = "Ile trzeba zebrać bierek żeby wygrać?",
                    CorrectAnswer = "Wartość zebranch bierek powinna być większa niż przeciwnika"
                },
            };

            var aswersEntries = new List<Answer>()
            {
                new Answer
                {
                    AnswerText = "Poker"
                },
                new Answer
                {
                    AnswerText = "Wartość zebranch bierek powinna być większa niż przeciwnika"
                },
            };

            var resultsEntries = new List<Result>()
            {
                new Result
                {
                    Score = 5,
                    Student_Id = 1,
                    TotalQuestions = 5
                },
                new Result
                {
                    Score = 3,
                    Student_Id = 1,
                    TotalQuestions = 7
                },
                new Result
                {
                    Score = 3,
                    Student_Id = 2,
                    TotalQuestions = 5
                }
            };

            context.Students.AddRange(studentEntries);
            context.Courses.AddRange(coursesEntries);
            context.Sections.AddRange(sectionEntries);
            context.Contents.AddRange(contentsEntries);
            context.Questions.AddRange(questionsEntries);
            context.Answers.AddRange(aswersEntries);
            context.Results.AddRange(resultsEntries);

            context.SaveChanges();

            base.Seed(context);
        }
    }
}
