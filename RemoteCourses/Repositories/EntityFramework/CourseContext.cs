﻿using RemoteCourses.Models;
using System.Data.Entity;

namespace RemoteCourses.Repositories.EntityFramework
{
    class CourseContext : DbContext
    {
        public CourseContext() : base("RemoteCoursesDb")
        {
            Database.SetInitializer(new CoursesDbInitializer());
        }

        public virtual DbSet<Student> Students { get; set; }
        public virtual DbSet<Course> Courses { get; set; }
        public virtual DbSet<Result> Results { get; set; }
        public virtual DbSet<Section> Sections { get; set; }
        public virtual DbSet<Question> Questions { get; set; }
        public virtual DbSet<Content> Contents { get; set; }
        public virtual DbSet<Answer> Answers { get; set; }
    }
}
