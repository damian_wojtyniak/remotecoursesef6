﻿using System.ComponentModel.DataAnnotations.Schema;

namespace RemoteCourses.Models
{
    class Section
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string SectionName { get; set; }

        public Course Courses { get; set; }
    }
}
