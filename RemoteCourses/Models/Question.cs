﻿using System.ComponentModel.DataAnnotations.Schema;

namespace RemoteCourses.Models
{
    class Question
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Text { get; set; }
        public string CorrectAnswer { get; set; }


        public Section Sections { get; set; }
    }
}
