﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace RemoteCourses.Models
{
    class Course
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public ICollection<Section> Section { get; set; }
        public ICollection<Student> Students { get; set; }
    }
}
