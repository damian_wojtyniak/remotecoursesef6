﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace RemoteCourses.Models
{
    class Result
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int Score { get; set; }
        public int TotalQuestions { get; set; }
        public DateTime? DateTaken { get; set; }

        [ForeignKey("Student")]
        public int Student_Id { get; set; }
        public virtual Student Student { get; set; }
        public virtual Section Sections { get; set; }
    }
}
